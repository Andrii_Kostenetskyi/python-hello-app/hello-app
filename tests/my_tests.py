import unittest
from hello_app.my_app import hello_app, load_json, q_redis
from hello_app.my_app import AppConfig
from unittest.mock import patch


class MyTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.cfg = AppConfig()
        cls.cfg.load('./tests/test-config.yml')

    def test_hello_app(self):
        """ URL that returns hard coded content. """

        pattern = r'<h1>Welcome to the Customer demo app\.</h1></hr>This is a first version -- running on [a-zA-Z0-9]+.'

        self.assertRegex(
            hello_app(),
            pattern
        )

    def test_json(self):
        """ URL that parses a JSON file and displays the content. """

        self.cfg.data['json']['path'] = './tests/testfile.json'

        self.assertEqual(
            load_json(),
            '\n<h1>JSON file content</h1><hr/>\n<b>The string value is</b>: Hello</br>\n<b>The list of numbers is:</b>\n<ul><li>1</li><li>2</li><li>3</li></ul>\n            '
        )

    @patch('redis.Redis.info')
    def test_redis(self, RedisMock):
        """ URL that does a INFO query on a redis database, and displays the result. """

        RedisMock.return_value = "{'a': 1}"

        self.assertEqual(
            q_redis(),
            '\n<h1>INFO query to redis DB running on 127.0.0.1:6379</h1></hr>\n<pre>"{\'a\': 1}"</pre>\n    '
        )


if __name__ == '__main__':
    unittest.main()
