Table of Contents
-----------------
1. [Description](#description)
2. [Pipeline](#pipeline)
3. [License](#license)
4. [Author](#author-information)

Description
=========

Hello APP is a project to demonstrate CI/CD via a simple app.
The application implements a simple webserver that serves a page with a simple message.

## Installation
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install
```bash
pip install hello-app
```
## Usage
```bash
$ source activate app-hello
$ hello-app
```

or via Docker
```bash
docker run -d --name hello-app -p 8080:8080 $DOCKER_REGISTRY/$APP_NAME
```

Pipeline
------------

A multistage gitlab-ci pipeline:
- setup stage:
  - dynamic installation of all necessary components
- test stage:
  - verification of the code based on flake8 (lint and syntax verification)
- unit_tests stage:
  - verification of the application by functional tests
- scan stage:
  - vulnerability scan based on SonarQube. Please, note that the configuration of projects/access tokens deliberately omitted.
- build stage: 
  - 1/2 creation of pip package (the result can be uploaded to pypi repos with [twine](https://pypi.org/project/twine/) tool, however currently this feature is commented in pipeline)
- publish stage: 
  - 2/2 the newly created docker image will be uploaded to ECR registry (where it will be scanned by default functionality of ECR service of AWS)
- docker-scan stage: 
  - will download the newly created docker image on dev VM (EC2) and will do security scanning with [trivy](https://github.com/aquasecurity/trivy) tool
- deploy stage:
  - deployment of application docker container with simple docker command on PROD VM (EC2)

Pipeline variables
------------
**APP_NAME** - the default name of application

**AWS_ACCESS_KEY_ID** - your access key for access via the technical account (tipoc002) for the deployments of resources on Amazon. This part is described in aws-infra repository
  
**AWS_SECRET_ACCESS_KEY** - your private key for access via the technical account (tipoc002) for the deployments of resources on Amazon. This part is described in aws-infra repository

**DOCKER_REGISTRY** - the name of ERC docker registry. This resource is deployed with aws-infra project

**SONAR_HOST_URL** - SonarQube tool can be placed not only on dev runners, but also somewhere else. By default, this values is: http://127.0.0.1:9000

**SONAR_TOKEN** - token for the link toward SonarQube project. 


License
-------

GNU GPL license


Author Information
------------------

Andrii KOSTENETSKYI




