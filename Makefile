SHELL := /bin/zsh

PYTHON_BIN=/opt/conda/envs/hello-app/bin


build-dev: test build push-dev clean

test:
	@echo "*** Running tests."
	${PYTHON_BIN}/pip install -i flake8 
	${PYTHON_BIN}/flake8 --count .

build:
	@echo "*** Creating a Python package."
	rm -rf `find . -name __pycache__`
	rm -f `find . -type f -name '.coverage' `
	rm -rf `find . -type d -name '.cache' `
	rm -rf dist
	rm -rf build
	rm -rf *.egg-info
	${PYTHON_BIN}/python setup.py sdist --formats=gztar


clean:
	@echo "*** Cleaning up."
	${PYTHON_BIN}/pip uninstall -y flake8 twine
	rm -rf `find . -name __pycache__`
	rm -f `find . -type f -name '.coverage' `
	rm -rf `find . -type d -name '.cache' `
	rm -rf dist
	rm -rf build
	rm -rf *.egg-info
