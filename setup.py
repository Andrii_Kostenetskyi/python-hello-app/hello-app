from setuptools import find_packages, setup


def get_version():
    with open('VERSION') as v:
        return v.readline().strip()


setup(
    name="hello_app",
    version=get_version(),
    packages=find_packages(),
    install_requires=[
        'Flask==1.1.1',
        'PyYAML==5.2',
        'redis==3.3.8',
        'requests==2.22.0'
    ],
    entry_points={
        'console_scripts': [
            'hello_app = hello_app.my_app:main',
        ]
    }
)
