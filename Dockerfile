FROM python:3.8-slim AS builder
WORKDIR /app
ENV PATH "/venv/bin:$PATH"
RUN python -m venv /venv
COPY requirements.pip.txt .
RUN pip install -r requirements.pip.txt

FROM python:3.8-alpine AS app
LABEL maintainer="medelgover@gmail.com"
WORKDIR /app
ENV PATH "/venv/bin:$PATH"
COPY --from=builder /venv /venv
COPY . .
EXPOSE 8080
CMD ["python", "hello_app/my_app.py"]
