#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from os import getpid
from flask import Flask
import json
import yaml
import redis
import pprint
import platform

app = Flask(__name__)


class AppConfig:

    _shared_state = {}

    def __init__(self):
        self.__dict__ = self._shared_state

    def load(self, filename):
        """
        load the configfile
        """
        with open(filename, 'r') as yf:
            self.data = yaml.load(yf, Loader=yaml.Loader)


@app.route('/')
def hello_app():
    """ Page with some hardcoded data. """
    content = """
<h1>Welcome to the Customer demo app.</h1></hr>This is a first version -- running on {}.
    """.format(
        platform.node()
    )

    return content


@app.route('/load_cfg')
def load_json():
    """ Load a basic JSON configfile and display the content. """

    cfg = AppConfig()

    try:
        with open(cfg.data['json']['path'], 'r') as jf:
            data = json.load(jf)

            msg = """
<h1>JSON file content</h1><hr/>
<b>The string value is</b>: {}</br>
<b>The list of numbers is:</b>
<ul>{}</ul>
            """.format(
                data['string'],
                "".join(['<li>{}</li>'.format(i) for i in data['numbers']])
            )

    except (IOError, FileNotFoundError):
        msg = "Error: Unable to read the json file."

    return msg


@app.route('/redis')
def q_redis():
    """
    Read some stuff from a redis DB.
    Redis details are in the configfile.
    """
    cfg = AppConfig()
    db_cfg = cfg.data.get('redis')

    r = redis.Redis(
        host=db_cfg.get('host', '127.0.0.1'),
        port=db_cfg.get('port', 6379),
        db=db_cfg.get('database', 0),
        password=db_cfg.get('password')
    )

    # Just return some info about the redis server we are querying and make it more readable
    msg = """
<h1>INFO query to redis DB running on {}</h1></hr>
<pre>{}</pre>
    """.format(":".join([db_cfg.get('host', '127.0.0.1'), str(db_cfg.get('port', 6379))]),
               pprint.pformat(r.info('server'), indent=4)
               )

    return msg


def main():
    """
    Main entry point.
    """

    with open('/tmp/hello-app.pid', 'w') as pf:
        pf.write("{}".format(getpid()))

    cfg = AppConfig()
    cfg_file = sys.argv[1] if len(sys.argv) > 1 else 'config.yml'
    cfg.load(cfg_file)
    app.run(
        host=cfg.data['main']['listen'],
        port=cfg.data['main']['port']
    )


if __name__ == "__main__":
    main()
